#include<stdio.h>

int input()
{
	int a;
	printf("Enter a number:\t");
	scanf("%d",&a);
	return a;
}

int fsum(int x)
{
	int temp;
	int sum=0;
	while(x!=0)
	{
		temp=x%10;
		sum=sum+temp;
		x=x/10;
	}
	return sum;
}

void output(int t, int s)
{
	printf("Sum of digits in %d=%d\n",t,s);
}

int main()
{
	int n, sum;
	n=input();
	sum=fsum(n);
	output(n,sum);
	return 0;
}

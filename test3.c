#include<stdio.h>

int input()
{
	int x;
	printf("Enter a number:");
	scanf("%d", &x);
	return x;
}

int compare(int x,int y,int z)
{
	int largest;
	largest=(x>y)?((x>z)?x:z):((y>z)?y:z);
	return largest;
}

void output(int x)
{
	printf("The largest number is %d\n", x);
}

int main()
{
	int a, b, c, m;
	a=input(); 
	b=input();
	c=input();
	m=compare(a,b,c);
	output(m);
	return 0;
}

#include<stdio.h>
void input_string(char str[])
{
	printf("Enter a string: ");
	gets(str);
}

int palindrome(char str[])
{
	int i,j,length,f;
	length=0,f=0;
	for(i=0;str[i]!='\0';i++)
	{
		length++;
	}
	for(j=0;j<length;j++)
	{
		if(str[j]!=str[length-j-1])
		{
			f=1;
			break;
		}
	}
	return f;
}

void output(char *str,int flag)
{
	if(flag==0)
	{
		printf("The string %s is a palindrome",str);
	}
	else
	{
		printf("The string %s is not a palindrome",str);
	}
}
int main()
{
	char str[100];
	int flag;
	input_string(str);
	flag=palindrome(str);
	output(str,flag);
	return 0;
}

#include<stdio.h>

int input()
{
	int a;
	scanf("%d", &a);
	return a;
}

int convert(int h,int m)
{
	int t;
	t = (h*60)+m;
	return t;
}	

void output(int a)
{
	printf("The total number of minutes= %d\n", a);
}


int main()
{
	int hr, min, totalmin;
	printf("Enter the number of hours:");
	hr = input();
	printf("Enter the number of minutes:");
	min = input();
	totalmin = convert(hr,min);
	output(totalmin);
	return 0;
}

	
	

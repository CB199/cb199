#include<stdio.h>
int input()
{
	int a;
	printf("Enter a number:\t");
	scanf("%d", &a);
	return a;
}

void mtable(int a[10])
{
	int i,j;
	for(i=1,j=0;i<=10;++i,j++)
	{
		a[j]=i;
	}
}

void output(int n,int a[10])
{
	int i;
	for(i=0;i<10;i++)
	{
	printf("%d x %d = %d\n",n,a[i],n*a[i]);
	}
}

int main()
{
	int a[10],n;
	n=input();
	mtable(a);
	output(n,a);
}	

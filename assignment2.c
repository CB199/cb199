#include<stdio.h>

float input()
{
	float a;
	scanf("%f", &a);
	return a;
}

float arear(float l1,float b1)
{
	float ar;
	ar = (l1*b1);
	return ar;
}

void output(float a)
{
	printf("Area of the rectangle=%.2f\n", a);
}

int main()
{
	float l, b, area;
	printf("Enter the length of the rectangle:");
	l = input();
	printf("Enter the breadth of the rectangle:");
	b = input();
	area = arear(l,b);
	output(area);
	return 0;
}

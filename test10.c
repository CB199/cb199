#include<stdio.h>

int main()
{
	FILE *fp;
	char c;
	printf("Data Input\n\n");
	fp=fopen("INPUT", "w");
	if(fp==NULL)
	{
		printf("File cannot be opened\n");
	}
	else
	{
		while((c=getchar()) != EOF)
		{
			puts(c,fp);
		}
		fclose(fp);
		
		printf("\nData Output\n\n");
		fp=fopen("INPUT","r");
		while((c=getc(fp)) != EOF)
		{
			printf("%c",c);
		}
		fclose(fp);
	}
}
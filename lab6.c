#include<stdio.h>

int input()
{
	int a;
	scanf("%d",&a);
	return a;
}

void input_matrix(int m,int n,int A[m][n])
{
	int i,j;
	printf("Enter the elements of the matrix:\n");
	for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			scanf("%d",&A[i][j]);
		}
		printf("\n");
	}
}

void transpose(int m,int n,int A[m][n],int B[n][m])
{
	int i,j;
	for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			B[j][i]=A[i][j];
		}
	}
}

void output(int n,int m,int B[n][m])
{
	int i,j;
	printf("Transpose of the matrix:\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
		{
			printf("%d\t",B[i][j]);
		}
		printf("\n");
	}
}

int main()
{
	int A[10][10],B[10][10],m,n;
	printf("Enter the dimensions of the matrix:");
	m=input();
	n=input();
	input_matrix(m,n,A);
	transpose(m,n,A,B);
	output(n,m,B);
	return 0;
}

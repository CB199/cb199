#include<stdio.h>
float input()
{
	float a;
	scanf("%f",&a);
	return a;
}

float simpleinterest(float a,float b, float c)
{
	float temp;
	temp=a*b*c/100;
	return temp;
}

void output(float a)
{
	printf("Simple Interest=%.2f\n", a);
}
	
int main()
{
	float p, r, t, si;
	printf("Principal Amount:");
	p=input();
	printf("Rate of Interest:");
	r=input();
	printf("Term:");
	t=input();
	si = simpleinterest(p,r,t);
	output(si);
	return 0;
}
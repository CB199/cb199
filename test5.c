#include<stdio.h>
int f_min(int n,int a[]);
int f_max(int n,int a[]);


int input()
{
	int a;
	printf("Enter the number of elements:\t");
	scanf("%d",&a);
	return a;
}

void input_array(int n,int a[])
{
	int i;
	printf("Enter the numbers:\n");
	for(i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
}

void sort_array(int n,int a[])
{
	int i,j,r;
	for(i=0;i<n;i++)
	{
		for(j=i+1;j<n;j++)
		{
			if(a[i]>a[j])
			{
				r=a[i];
				a[i]=a[j];
				a[j]=r;
			}
		}
	}
	printf("The numbers arranged in ascending order:\n");
	for(i=0;i<n;i++)
	{
		printf("%d\t",a[i]);
	}	
}

void output(int n,int a[])
{
	int i;
	printf("\nAfter swapping\n");
	for(i=0;i<n;i++)
	{
		printf("%d\t",a[i]);
	}
}

int main()
{
	int a[10],n,temp,max,min;
	n=input();
	input_array(n,a);
	sort_array(n,a);
	min=f_min(n,a);
	max=f_max(n,a);
	
	temp=a[min];
	a[min]=a[max];
	a[max]=temp;
	
	output(n,a);
	return 0;
}


int f_min(int n1,int a[])
{
 	int t,i;
	t=0;
	for(i=1;i<n1;i++)
	{
		if(a[i]<a[t])
		{
			t=i;
		}
	}
	return t;
}

int f_max(int n1,int a[])
{
 	int t,i;
	t=0;
	for(i=1;i<n1;i++)
	{
		if(a[i]>a[t])
		{
			t=i;
		}
	}
	return t;
}

#include<stdio.h>
int main()
{
	int x,y,temp;
	int *n1, *n2;
	printf("Enter the values of x and y = ");
	scanf("%d%d", &x, &y);
	n1 = &x;
	n2 = &y;
	
	printf("\nBefore swapping:\n");
	printf("Value of x = %d\nValue of y = %d",x,y);
	
	temp = *n1;
	*n1 = *n2;
	*n2 = temp;

	printf("\nAfter swapping\n");
	printf("Value of x = %d\nValue of y = %d",x,y);
	return 0;
}



	
	


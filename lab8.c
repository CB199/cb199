#include<stdio.h>
struct student
{
	int roll;
	char name[50];
	char section;
	char dept[10];
	float fees;
	float totmarks;
};
typedef struct student std;

int main()
{
	std s[100];
	int i,n;
	float max;
	printf("Enter the total number of students:");
	scanf("%d",&n);
	
	for(i=0;i<n;i++)
	{
		printf("\n");
		printf("Enter Roll Number:");
		scanf("%d",&s[i].roll);
		printf("Enter Name:");
		scanf("%s",s[i].name);
		printf("Enter Section:");
		scanf("%c",&s[i].section);
		printf("Enter Department:");
		gets(s[i].dept);
		printf("Enter Fees:");
		scanf("%f",&s[i].fees);
		printf("Enter Total Marks:");
		scanf("%f",&s[i].totmarks);
	}
	
	max=s[0].totmarks;
	for(i=0;i<n;i++)
	{
		if(max<s[i].totmarks)
		{
			max=s[i].totmarks;
		}
	}
	
	printf("\nDetails of student who scored max marks:\n");
	for(i=0;i<n;i++)
	{
		if(s[i].totmarks==max)
		{
			printf("Roll Number: %d\n",s[i].roll);
			printf("Name: %s\n",s[i].name);
			printf("Section: %c\n",s[i].section);
			printf("Department: %s\n",s[i].dept);
			printf("Fees: %.2f\n",s[i].fees);
			printf("Total Marks: %.2f\n",s[i].totmarks);
		}
	}
	return 0;
}

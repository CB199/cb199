#include<stdio.h>
int input()
{
	int a;
	printf("Enter the number of elements: ");
	scanf("%d",&a);
	return a;
}

void input_array(int n,int a[])
{
	int i;
	printf("Enter the numbers: ");
	for(i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
}

float compute(int n,int a[])
{
	int i,sum=0;
	float average;
	for(i=0;i<n;i++)
	{
		sum = sum+a[i];
	}
	average = (float)sum/n;
	return average;
}

void output(int n,float avg)
{
	printf("Average of the %d numbers = %.2f",n,avg);
}

int main()
{
	int a[10],n;
	float avg;
	n=input();
	input_array(n,a);
	avg=compute(n,a);
	output(n,avg);
	return 0;
}

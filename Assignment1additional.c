#include<stdio.h>

float input()
{
	float a;
	printf("Enter the temperature in Fahrenheit:");
	scanf("%f", &a);
	return a;
}

float temp(float t)
{
	float z;
	z = (t-32)*5/9;
	return z;
}

void output(float a)
{
	printf("Temperature in Degree Celsius=%.2f", a);
}

int main()
{
	float f, c;
	f = input();
	c = temp(f);
	output(c);
	return 0;
}

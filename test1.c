#include<stdio.h>
#include<math.h>
struct point
{
    float x;
    float y;
};
typedef struct point pnt;

pnt input()
{
    pnt p;
    printf("Enter the x-coordinate:");
    scanf("%f",&p.x);
    printf("Enter the y-coordinate:");
    scanf("%f",&p.y);
    return p;
}

float compute(pnt p1,pnt p2)
{
    float dist;
    dist=sqrt(((p1.x-p2.x)*(p1.x-p2.x))+((p1.y-p2.y)*(p1.y-p2.y)));
    return dist;
}

void output(pnt p1,pnt p2,float distance)
{
    printf("Distance between the points (%.2f,%.2f)&(%.2f,%.2f)=%.2f",p1.x,p1.y,p2.x,p2.y,distance);
}
 
int main()
{
    float d;
    pnt p1,p2;
    printf("Coordinates of the first point:\n");
    p1=input();
    printf("Coordinates of the second point:\n");
    p2=input();
    d=compute(p1,p2);
    output(p1,p2,d);
    return 0;
}

#include<stdio.h>
int main()
{
	int num1,num2,sum,diff,multiply,rem;
	float div;
	int *x, *y;
	x=&num1;
	y=&num2;
	printf("Enter any two numbers:");
	scanf("%d%d",x,y);
	sum = (*x) + (*y);
	diff = (*x) - (*y);
	multiply = (*x) * (*y);
	div = (*x) / (float)(*y);
	rem = (*x) % (*y);
	printf("\nSum of %d and %d = %d\n", *x, *y, sum);
	printf("Difference between %d and %d = %d\n", *x, *y, diff);
	printf("Product of %d and %d = %d\n", *x, *y, multiply);
	printf("Quotient of %d and %d = %.2f\n", *x, *y, div);
	printf("Remainder of %d and %d = %d", *x, *y, rem);
	return 0;
}

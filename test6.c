#include<stdio.h>
int input()
{
	int a;
	scanf("%d",&a);
	return a;
}

void input_marks(int m,int n,int marks[][n])
{
	int i,j;
	for(i=0;i<m;i++)
	{
		printf("Enter the marks of student %d in %d subjects:",i+1,n);
		for(j=0;j<n;j++)
		{
			scanf("%d",&marks[i][j]);
		}
	}
}

void max_marks(int m,int n,int marks[][n],int maxmarks[])
{
	int i,j;
	for(i=0;i<m;i++)
	{
		maxmarks[i]=0;
		for(j=0;j<n;j++)
		{
			if(marks[i][j]>maxmarks[i])
			{
				maxmarks[i]=marks[i][j];
			}
		}
	}
}

void output(int m,int maxmarks[])
{
	int i;
	for(i=0;i<m;i++)
	{
		printf("Maximum marks of the %d student = %d",i+1,maxmarks[i]);
		printf("\n");
	}
	
}

int main()
{
	int marks[10][10],maxmarks[10],m,n;
	printf("Enter the number of students:");
	m=input();
	printf("Enter the number of subjects:");
	n=input();
	input_marks(m,n,marks);
	max_marks(m,n,marks,maxmarks);	
	output(m,maxmarks);
	return 0;
}

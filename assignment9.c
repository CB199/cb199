#include<stdio.h>
struct details
{
	char name[100];
	int id;
	float salary;
};
typedef struct details employees;

int main()
{
	employees e[10];
	int i,n;
	
	printf("Enter the number of employees: ");
	scanf("%d",&n);
	
	printf("Enter the %d employee details:\n",n);
	for(i=0;i<n;i++)
	{
		printf("\nDetails of Employee %d:\n",i+1);
		printf("Enter the name of the employee:");
		scanf("%s",e[i].name);
		printf("Enter the ID no. of the employee:");
		scanf("%d",&e[i].id);
		printf("Enter the salary of the employee:");
		scanf("%f",&e[i].salary);
	}
	
	
	printf("\nDetails of the employees whose salary is less than INR25000/-\n");
	for(i=0;i<n;i++)
	{
		if(e[i].salary<=25000)
		{
			printf("\nName: %s\n",e[i].name);
			printf("ID: %d\n",e[i].id);
			printf("Salary: %.2f\n",e[i].salary);
			printf("\n");
		}
	}
	return 0;
}

#include<stdio.h>

int input()
{
	int x;
	printf("Enter the number of elements: ");
	scanf("%d",&x);
	return x;
}

void input_array(int n,int a[])
{
	int i;
	printf("Enter the %d numbers: ",n);
	for(i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
}

float compute(int n,int a[])
{
	int i,sum=0;
	float average;
	for(i=0;i<n;i++)
	{
		sum+=a[i];
	}
	average = (float)sum/n;
	return average;
}

void output(int n,int a[],float avg)
{
	int i;
	printf("\nMean of the %d numbers = %.2f",n,avg);
	printf("\nThe numbers greater than the mean of the numbers:\t");
	for(i=0;i<n;i++)
	{
		if(a[i]>avg)
		{
			printf("%d\t",a[i]);
		}
	}
}

int main()
{	
	int a[10],n;
	float avg;
	n=input();
	input_array(n,a);
	avg=compute(n,a);
	output(n,a,avg);
	return 0;
}

#include<stdio.h>
int input()
{
	int a;
	printf("Enter the number of elements of the array:");
	scanf("%d",&a);
	return a;
}

void input_array(int n,int arr[])
{
	int i;
	printf("Enter the elements of the array:\n");
	for(i=0;i<n;i++)
	{
		scanf("%d",&arr[i]);
	}
}

void sort_array(int n,int arr[],int even_arr[],int odd_arr[],int a,int b)
{
	int i;
	for(i=0;i<n;i++)
	{
		if(arr[i]%2==0)
		{
			even_arr[a]=arr[i];
			a++;
		}
		else
		{
			odd_arr[b]=arr[i];
			b++;
		}
	}
}

void output(int even_arr,int odd_arr,int a,int b)
{
	int i;
	printf("Even elements:\n");
	for(i=0;i<a;i++)
	{
		printf("%d\t",even_arr[i]);
	}
	
	printf("\nOdd elements:\n");
	for(i=0;i<b;i++)
	{
		printf("%d\t",odd_arr[i]);
	}
}

int main()
{
	int arr[10],odd_arr[10],even_arr[10],n,a=0,b=0;
	input();
	input_array(n,arr);
	sort_array(n,arr,even_arr,odd_arr,a,b);
	output(even_arr,odd_arr,a,b);
	return 0;
}

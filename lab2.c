#include<stdio.h>
float areac(float);

float input()
{
	float rad;
	printf("Enter the radius of the circle:");
	scanf("%f", &rad);
	return rad;
}

float areac(float rad)
{
	float pi=3.14,a;
	a=pi*rad*rad;
	return a;
}

void output(float r,float a)
{
	printf("Area of the circle of radius %f = %.2f\n",r,a);
}

int main()
{
	float r,area;
	r=input();
	area = areac(r);
	output(rad,area);
	return 0;
}
	
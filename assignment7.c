#include<stdio.h>
int main()
{
	int a[10][10],n,i,j,s1,s2,s3;
	s1=s2=s3=0;
	printf("Enter the number of rows and columns of the matrix(nxn):");
	scanf("%d",&n);
	
	printf("Enter the elements of the matrix:\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			scanf("%d",&a[i][j]);
		}
	}
	
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			if(i==j)
			{
				s1=s1+a[i][j];
			}
			else if(j<i)
			{
				s2=s2+a[i][j];
			}
			else if(j>i)
			{
				s3=s3+a[i][j];
			}
		}
	}
	
	printf("\nThe sum of elements along the diagonal of the matrix:%d\n",s1);
	printf("The sum of elements below the diagonal of the matrix:%d\n",s2);
	printf("The sum of elements above the diagonal of the matrix:%d",s3);
	
	return 0;
}
